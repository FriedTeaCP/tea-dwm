void warp(const Arg *arg) {
  Client *c = selmon->sel;
  if (c != NULL) {
    XWarpPointer(dpy, None, c->win, 0, 0, 0, 0, c->w / 2, c->h / 2);
  }
}
