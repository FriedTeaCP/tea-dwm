/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 2;        /* border pixel of windows */
static const unsigned int gappx     = 10;       /* gap pixel between windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int focusonwheel       = 0;
static const int user_bh            = 35;        /* 0 means that dwm will calculate bar height, >= 1 means dwm will user_bh as bar height */
static const char *fonts[]          = { "JetBrains Mono:size=11", "Symbols Nerd Font:size=11"};
static const char col_bg[]          = "#191E1E";
static const char col_fg[]          = "#C5B7AD";
static const char col_fg2[]         = "#3F4B4B";
static const char *colors[][3]      = {
	/*                  text/fg     bg     border   */
	[SchemeNorm]     = { col_fg,  col_bg,  col_bg },
	[SchemeSel]      = { col_fg,  col_bg,  col_fg },
	[SchemeStatus]   = { col_fg,  col_bg,  "#000000" }, // Statusbar right
	[SchemeTagsSel]  = { col_fg,  col_bg,  "#000000" }, // Tagbar left selected
	[SchemeTagsNorm] = { col_fg2, col_bg,  "#000000" }, // Tagbar left unselected
	[SchemeInfoSel]  = { col_bg,  col_bg,  "#000000" }, // infobar middle  selected
	[SchemeInfoNorm] = { col_bg,  col_bg,  "#000000" }, // infobar middle  unselected
};

/* tagging */
static const char *tags[] = {"", "", "", ""};
static const char *alttags[] = {"", "", "", ""};

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class           instance  title           tags mask  isfloating monitor */
	{ "st-256color",    NULL,     NULL,              0,         0,       -1 },
	{ NULL,             NULL,     "fzf-emoji",       0,         1,       -1 },
	{ NULL,             NULL,     "Event Tester",    0,         0,       -1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
#define tmux_init "command -v tmux && (tmux has-session && ($TERMINAL -e tmux attach; exit 0) || ($TERMINAL -e tmux new-session -s Default; exit 0)) || $TERMINAL"

#include "movestack.c"
#include "cursorwarp.c"
static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_w,      warp,           {0} },
	{ MODKEY,                       XK_p,      spawn,          SHCMD("dmenu_run -h 35 -l 0 -p 'Execute:'") },
	{ MODKEY,                       XK_Return, spawn,          SHCMD(tmux_init) },
	{ MODKEY|ShiftMask,             XK_Return, spawn,          SHCMD("$TERMINAL") },
	{ MODKEY|ShiftMask,             XK_b,      togglebar,      {0} },

	{ MODKEY|ControlMask,           XK_j,      incnmaster,     {.i = +1 } },
	{ MODKEY|ControlMask,           XK_k,      incnmaster,     {.i = -1 } },

	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = +1 } },

	{ MODKEY,                       XK_j,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = +1 } },

	{ MODKEY|ShiftMask,             XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY|ShiftMask,             XK_l,      setmfact,       {.f = +0.05} },

	{ MODKEY,                       XK_space,  zoom,           {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,             XK_c,      killclient,     {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },


	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },

	{ MODKEY|ControlMask,           XK_comma,   focusmon,      {.i = -1 } },
	{ MODKEY|ControlMask,           XK_period,  focusmon,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,   tagmon,        {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period,  tagmon,        {.i = +1 } },


	{ MODKEY,                       XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,                       XK_t,	   setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,             XK_f,      togglefullscr,  {0} },

	{ MODKEY,                       XK_Down,   moveresize,     {.v = "0x 25y 0w 0h" } },
	{ MODKEY,                       XK_Up,     moveresize,     {.v = "0x -25y 0w 0h" } },
	{ MODKEY,                       XK_Right,  moveresize,     {.v = "25x 0y 0w 0h" } },
	{ MODKEY,                       XK_Left,   moveresize,     {.v = "-25x 0y 0w 0h" } },
	{ MODKEY|ShiftMask,             XK_Down,   moveresize,     {.v = "0x 0y 0w 25h" } },
	{ MODKEY|ShiftMask,             XK_Up,     moveresize,     {.v = "0x 0y 0w -25h" } },
	{ MODKEY|ShiftMask,             XK_Right,  moveresize,     {.v = "0x 0y 25w 0h" } },
	{ MODKEY|ShiftMask,             XK_Left,   moveresize,     {.v = "0x 0y -25w 0h" } },

	{ MODKEY,                       XK_bracketright,   spawn,    SHCMD("pactl set-sink-volume @DEFAULT_SINK@ +5%; kill -49 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_bracketleft,    spawn,    SHCMD("pactl set-sink-volume @DEFAULT_SINK@ -5%; kill -49 $(pidof dwmblocks)") },
	{ MODKEY|ControlMask,           XK_bracketright,   spawn,    SHCMD("pactl set-sink-volume @DEFAULT_SINK@ +1%; kill -49 $(pidof dwmblocks)") },
	{ MODKEY|ControlMask,           XK_bracketleft,    spawn,    SHCMD("pactl set-sink-volume @DEFAULT_SINK@ -1%; kill -49 $(pidof dwmblocks)") },
	{ MODKEY,                       XK_backslash,      spawn,    SHCMD("pactl set-sink-mute @DEFAULT_SINK@ toggle; kill -49 $(pidof dwmblocks)") },

	{ MODKEY,                       XK_b,      spawn,          SHCMD("$BROWSER & herbe \"Opening $BROWSER\"") },
	{ MODKEY,                       XK_c,      spawn,          SHCMD("pkill xcompmgr || xcompmgr") },
	{ MODKEY,                       XK_d,      spawn,          SHCMD("dmenu-doc -x 200 -z 570 -h 35") },
	{ MODKEY,                       XK_u,      spawn,          SHCMD("fzf-emoji -t") },
	{ MODKEY|ControlMask,           XK_f,      spawn,          SHCMD("$TERMINAL -e bash -c 'source ~/.bashrc && n'") },
	{ MODKEY,                       XK_n,      spawn,          SHCMD("$TERMINAL -t nmtui -e nmtui") },
	{ MODKEY,                       XK_v,      spawn,          SHCMD("$TERMINAL -e pulsemixer") },
	{ MODKEY,                       XK_s,      spawn,          SHCMD("screenshot -y 35 -z 177 -h 24 -i -l 3") },
	{ MODKEY|ShiftMask,             XK_s,      spawn,          SHCMD("screenshot -t -y 35 -z 177 -h 24 -i -l 3") },
	{ MODKEY,                       XK_q,      spawn,          SHCMD("dmenu-sys -y 35 -z 177 -h 24 -i") },
	{ MODKEY|ShiftMask,             XK_r,      spawn,          SHCMD("dmenu-resolution") },

	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} }, /* Quit dwm */
	{ MODKEY|ControlMask,           XK_r,      quit,           {1} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          SHCMD("$TERMINAL") },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

